package engant.example.webintentbasic

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class WebIntentTest {
    @Test
    @Throws(Exception::class)
    fun firstUnitTest() {
        //assertFalse("This shouldn't work!", MainActivity.validateUrl("uefa.com"))
        assertFalse("This shouldn't work!", MainActivity.validateUrl(""))

    }

    @Test
    @Throws(Exception::class)
    fun secondUnitTest() {
        assertTrue("This should work!", MainActivity.validateUrl("http://uefa.com"))
    }
}