package engant.example.webintentbasic

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /** when the "Go" button is clicked */
    fun startWebIntent(go_button: View) {
        val url_editText = findViewById(R.id.in_url) as EditText
        val web_intent = Intent(Intent.ACTION_VIEW)
        //TODO: ??? use createChooser()

        val uriText = url_editText.text.toString().trim()
        if (MainActivity.validateUrl(uriText)) {
            web_intent.data = Uri.parse(uriText)
            startActivity(web_intent)
        }
    }

    companion object Tester {

        @JvmStatic
        fun validateUrl(url: String?): Boolean {
            /* TODO - NullException???
            val p = Patterns.WEB_URL
            //if (!uri.isNullOrBlank()) {
            val m = p.matcher(url?.toLowerCase())
            return m.matches()
            //}
            //return false*/

            if (url.isNullOrBlank())
                return false
            return true
        }

        /*@JvmStatic
        fun validateUrl(url: String?): Boolean? {
            val proto = "(http|https|ftp)"
            val pattern = fun (): String = """^${proto}"""
            
            val lower = url?.toLowerCase()
            return lower?.matches( pattern().toRegex() )
        }*/
    }
}
